<?php

/**
 * @file
 * Entity-related functions.
 */

/**
 * Implements hook_entity_info().
 */
function total_subscription_entity_info() {
  $return = array(
    'total_subscription' => array(
      'label' => t('Total Subscription'),
      'description' => t('An entity type used by the Total Subscription module.'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'total_subscription',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'tsid',
      ),
      'label callback' => 'total_subscription_entity_label',
      'uri callback' => 'total_subscription_entity_uri',
      'module' => 'total_subscription',
    ),
  );
  return $return;
}

/**
 * Implements of callback_entity_info_label().
 */
function total_subscription_entity_label($entity) {
  return implode(', ', array(
    $entity->mail,
    $entity->entity_type,
    $entity->bundle,
    $entity->entity_id,
  ));
}

/**
 * Implements of callback_entity_info_uri().
 */
function total_subscription_entity_uri($entity) {
  return array(
    'path' => ($entity->entity_type == 'taxonomy_term' ? '/taxonomy/term/' : '/node/') . $entity->entity_id,
  );
}
