<?php

/**
 * @file
 * Admin settings for Total Subscription module.
 */

/**
 * Callback function for Admin settings for Mails (Mail Settings).
 */
function total_subscription_mail_admin_form_settings() {
  $form = array();
  $form['total_subscription_email_address'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('total_subscription_email_address', ''),
    '#description' => t('Please specify the email address.'),
    '#title' => t('Email address'),
  );
  $form['total_subscription_subject_published'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('total_subscription_subject_published', ''),
    '#description' => t('Please specify the subject when some content will published.'),
    '#title' => t('Subject when content is published'),
  );
  $form['total_subscription_subject_subscription'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('total_subscription_subject_subscription', ''),
    '#description' => t('Please specify the subject when someone subscribes.'),
    '#title' => t('Subscription subject'),
  );
  $form['total_subscription_node_update'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('total_subscription_node_update', 0),
    '#description' => t('Whether to receive an email if the node has changed
    (if the checkbox is disabled, only new and switched to published nodes will be emailed).'),
    '#title' => t('Subscribe for updates'),
  );
  $total_subscription_shorten_url = t('Please check this if you want to shorten subscribe/unsubscribe urls.') .
    ' ' . l(t('Shorten URLs'), 'http://drupal.org/project/shorten') . ' ' . t('module required.');
  $form['total_subscription_shorten_url'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('total_subscription_shorten_url', 0),
    '#title' => t('Shorten the URLs'),
    '#description' => $total_subscription_shorten_url,
    '#disabled' => !module_exists('shorten'),
  );

  return system_settings_form($form);
}

/**
 * Callback function for Admin settings for Mails (Templates).
 */
function total_subscription_admin_form() {
  $form = array();
  $form['total_subscription_send_start_template'] = array(
    '#type' => 'textarea',
    '#default_value'    => variable_get('total_subscription_send_start_template') ? variable_get('total_subscription_send_start_template') : '',
    '#description'      => t('Use Subscription token [subscription:subscribe] to add subscribe link and [subscription:unsubscribe-page] as link to unsubscribe page.'),
    '#title'            => t('Subscribe e-mail template'),
  );
  $form['total_unsubscription_send_start_template'] = array(
    '#type'             => 'textarea',
    '#default_value'    => variable_get('total_unsubscription_send_start_template') ? variable_get('total_unsubscription_send_start_template') : '',
    '#description'      => t('Use Subscription token [subscription:unsubscribe] to add unsubscribe link and [subscription:unsubscribe-page] as link to unsubscribe page.'),
    '#title'            => t('Unsubscribe e-mail template'),
  );
  $form['body_start_template'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('Content e-mail template'),
    '#description'      => t('Fill this template to send e-mails to your subscribers.'),
  );
  $form['body_start_template']['total_subscription_email_template'] = array(
    '#type'             => 'textarea',
    '#default_value'    => variable_get('total_subscription_email_template') ? variable_get('total_subscription_email_template') : '',
    '#description'      => t('Use Subscription token [subscription:unsubscribe-page] as link to unsubscribe page.'),
    '#title'            => t('Content template'),
    '#rows'             => 20,
  );
  $form['body_start_template']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['body_start_template']['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node'),
  );
  $form['body_start_template']['total_subscription_html_templates'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('total_subscription_html_templates', 0),
    '#title' => t('Use HTML templates'),
    '#description' => t('You can send HTML e-mails if') .
    ' ' . l(t('Mime Mail'), 'http://drupal.org/project/mimemail') . ' ' . t('module installed.'),
    '#disabled' => !module_exists('mimemail'),
  );

  return system_settings_form($form);
}

/**
 * Callback function for Admin settings for Mails (AND Block Filtering).
 */
function total_subscription_filtering_form() {
  $form = array();

  $form['set1'] = array(
    '#type' => 'fieldset',
    '#title' => t('First vocabularies set'),
    '#description' => t('Choose options for first vocabularies set.'),
    '#collapsible' => TRUE,
  );

  $form['set1']['total_subscription_title1'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('total_subscription_title1', ''),
    '#description' => t('Choose title for first vocabulary set.'),
  );

  $form['set1']['total_subscription_set1'] = array(
    '#type' => 'select',
    '#title' => t('Set'),
    '#multiple' => TRUE,
    '#options' => total_subscription_get_all_vocs(),
    '#default_value' => variable_get('total_subscription_set1', ''),
    '#description' => t('Choose set of vocabularies.'),
  );

  $form['set1']['total_subscription_level1'] = array(
    '#type' => 'select',
    '#title' => t('Max level'),
    '#multiple' => FALSE,
    '#options' => array(
      '- No restriction -',
      1, 2, 3, 4, 5, 6, 7, 8, 9,
    ),
    '#default_value' => variable_get('total_subscription_level1', ''),
    '#description' => t('Choose level constraint for vocabularies.'),
  );

  $form['set1']['total_subscription_custom1'] = array(
    '#type' => 'textfield',
    '#title' => t('Disabled terms'),
    '#default_value' => variable_get('total_subscription_custom1', ''),
    '#description' => t('Term IDs which should be removed from set, separated by commas.'),
  );

  $form['set2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Second vocabularies set'),
    '#description' => t('Choose options for second vocabularies set.'),
    '#collapsible' => TRUE,
  );

  $form['set2']['total_subscription_title2'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('total_subscription_title2', ''),
    '#description' => t('Choose title for second vocabulary set.'),
  );

  $form['set2']['total_subscription_set2'] = array(
    '#type' => 'select',
    '#title' => t('Set'),
    '#multiple' => TRUE,
    '#options' => total_subscription_get_all_vocs(),
    '#default_value' => variable_get('total_subscription_set2', ''),
    '#description' => t('Choose set of vocabularies.'),
  );

  $form['set2']['total_subscription_level2'] = array(
    '#type' => 'select',
    '#title' => t('Max level'),
    '#multiple' => FALSE,
    '#options' => array(
      '- No restriction -',
      1, 2, 3, 4, 5, 6, 7, 8, 9,
    ),
    '#default_value' => variable_get('total_subscription_level2', ''),
    '#description' => t('Choose level constraint for vocabularies.'),
  );

  $form['set2']['total_subscription_custom2'] = array(
    '#type' => 'textfield',
    '#title' => t('Disabled terms'),
    '#default_value' => variable_get('total_subscription_custom2', ''),
    '#description' => t('Term IDs which should be removed from set, separated by commas.'),
  );

  return system_settings_form($form);
}

/**
 * Validate admin AND filtering form.
 */
function total_subscription_filtering_form_validate($form, &$form_state) {
  $set1 = $form_state['values']['total_subscription_set1'];
  $set2 = $form_state['values']['total_subscription_set2'];

  foreach ($set1 as $voc) {
    if (in_array($voc, $set2)) {
      $vocs = taxonomy_vocabulary_load_multiple(array($voc));
      form_set_error('total_subscription_set1',
        t('You have same vocabulary "@voc" in both sets, please exclude it from any.', array('@voc' => $vocs[$voc]->name)));
    }
  }
}

/**
 * Implements callback for taxonomy search.
 */
function total_subscription_taxonomy_search() {
  $view = views_get_view('total_subscription');
  $view->set_display('block');
  $view->pre_execute();
  $view->execute();
  return $view->render();
}

/**
 * Implements callback for node search.
 */
function total_subscription_node_search() {
  $view = views_get_view('total_subscription');
  $view->set_display('block_1');
  // Change the amount of items to show.
  $view->pre_execute();
  $view->execute();
  return $view->render();
}
