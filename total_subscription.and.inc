<?php

/**
 * @file
 * AND filter - related functions.
 */

/**
 * Callback of total_subscription_and_block().
 */
function total_subscription_and_block() {
  return drupal_get_form('total_subscription_subscribe_and_form', arg());
}

/**
 * Form for the 'AND' filtered subscriptions.
 */
function total_subscription_subscribe_and_form($form, &$form_state, $contexts) {
  global $user;

  $form['set1'] = array(
    '#type' => 'select',
    '#title' => variable_get('total_subscription_title1', ''),
    '#multiple' => FALSE,
    '#options' => total_subscription_get_filtered_tids(1),
  );

  $form['set2'] = array(
    '#type' => 'select',
    '#title' => variable_get('total_subscription_title2', ''),
    '#multiple' => FALSE,
    '#options' => total_subscription_get_filtered_tids(2),
  );

  $form['entity_type'] = array(
    '#type' => 'hidden',
    '#name' => 'entity_type',
    '#value' => 'taxonomy_term',
  );

  // Checking for logged-in user.
  if (user_is_logged_in()) {
    $form['mail'] = array(
      '#type' => 'hidden',
      '#value' => $user->mail,
    );
  }
  else {
    $form['mail'] = array(
      '#type' => 'textfield',
      '#attributes' => array(
        'class' => array('total_subscription_email'),
        'placeholder' => array(t('Please enter your Email.')),
        'onkeydown' => 'javascript: jQuery(this).parent().next().show();',
      ),
      '#size' => 25,
      '#maxlength' => 128,
    );
  }

  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );

  return $form;
}

/**
 * Form validation.
 */
function total_subscription_subscribe_and_form_validate($form, &$form_state) {
  $check = db_select('total_subscription', 'ti');
  $check->fields('ti', array('hash'))
    ->condition('ti.entity_type', 'taxonomy_term')
    ->condition('ti.entity_id', $form_state['values']['set1'])
    ->condition('ti.entity_id_and', $form_state['values']['set2'])
    ->condition('ti.mail', $form_state['values']['mail']);
  $result = $check->execute()->fetchAll();
  if (count($result) > 0) {
    form_set_error('set1', t('Subscription already exist.'));
  }
}

/**
 * Get filtered tids for AND block.
 */
function total_subscription_get_filtered_tids($idx) {
  $set = variable_get('total_subscription_set' . $idx, '');
  $level = variable_get('total_subscription_level' . $idx, '');
  $custom = variable_get('total_subscription_custom' . $idx, '');

  $set = array_keys($set);
  if ($level == 0) {
    $level = NULL;
  }
  $custom = explode(',', $custom);

  $tids_result = array();
  foreach ($set as $voc) {
    $items = taxonomy_get_tree($voc, 0, $level);
    foreach ($items as $item) {
      if (!in_array($item->tid, $custom)) {
        $dashes = '';
        for ($i = 0; $i < $item->depth; $i++) {
          $dashes .= '--';
        }
        $tids_result[$item->tid] = $dashes . $item->name;
      }
    }
  }
  return $tids_result;
}

/**
 * Submit callback for Subscription AND Block.
 */
function total_subscription_subscribe_and_form_submit($form, &$form_state) {
  global $user;
  global $base_url;
  $hash = md5(rand(0, 1000));
  $form_state['values']['hash'] = $hash;

  $set1 = $form_state['values']['set1'];
  $set2 = $form_state['values']['set2'];

  $form_state['values']['entity_id'] = $set1 . '+' . $set2;

  $taxonomy_detail1 = taxonomy_term_load($set1);
  $taxonomy_detail2 = taxonomy_term_load($set2);

  _total_subscription_insert_record(array(
    'mail' => $form_state['values']['mail'],
    'entity_type' => 'taxonomy_term',
    'bundle' => $taxonomy_detail1->name,
    'bundle_and' => $taxonomy_detail2->name,
    'entity_id' => $set1,
    'entity_id_and' => $set2,
    'hash' => $hash,
    'content_type_name' => 1,
  ), $user);

  if ($user->uid == 0) {
    // Sending mails.
    total_subscription_mail_send($form_state['values']);
    drupal_set_message(t('Thank you for subscribing. Please check your email for steps to verify your account.'), 'status', FALSE);
  }
  return 'and';
}
