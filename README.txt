README
======

  Total Subscription provides functionality that allows the users to subscribe
  to node pages, taxonomy terms, etc. The main feature which distinguished it
  from other subscription modules is that it allows subscription for Anonymous
  along with Authenticated users.

Total Subscription provides users the ability to:

  1. Subscribe to a specific node through the node page.
  2. Subscribe to the related taxonomy terms associated with the node on the
     node page.
  3. Subscribe to a specific taxonomy term from the term page.
  4. Subscribe to any taxonomy term from the front page.
  5. Unsubscribe from any previous subscription. You can unsubscribe using
     special page, or via admin UI, if you have appropriate permission.
  6. Add a separate CTools content type - "total_subscription", which could be
     integrated with any panel page.
  7. Add a "subscription" block to the any region.
  8. Add a "AND subscription" block to the any region.

AND block allows to subscribe on two terms from different vocabularies. Users
will got emails only if node status with both terms attached is changed.

Dependencies:

  1. CTools
  2. Token
  3. Views

You also can use this modules (these dependencies is not required, but
recommended):

  1. Views Bulk Operations - to delete several subscriptions at once.

    If this module absent, you probably can see warnings in logs.

  2. Mime Mail - to send HTML content emails.

    For notification emails, only plain text is allowed, to prevent complex
    spam letters.
    You can use any tokens in content emails, no matter HTML or plain text
    content letters you want to send.
    The key for notification mails for anonymous user subscription is
    "total_subscription_message".
    The key for sending mail while content publication is
    "total_subscription_node_message".

  3. Shorten URLs - to shorten subscribe/unsubscribe urls.

    By default, TinyURL service is used. You can configure any shortening
    provider.

  4. CAPTCHA - to prevent anonymous subscribing by bots.

Emails will be triggered thrice in the process:

  1. During anonymous subscription.
  2. After content published (or changed).
  3. During unsubscription via special page.

  Notification letters are sent immediately, but content letters is sent
  only by cron.

Upgrade from previous version:

  * You need to update your views. Enable Views UI module, and click
  "Revert" operation on views list page to upgrade views.
  * You need to execute drush updb.

Future Plans:

  1. Expecting feature requests from community.
  2. Porting this module to Drupal 8.

(C) Andrew Answer http://answe.ru 2019
