<?php

/**
 * @file
 * Token-related hooks.
 */

/**
 * Implements hook_token_info().
 */
function total_subscription_token_info() {
  $info['tokens']['subscription']['subscribe'] = array(
    'name' => t('Subscribe'),
    'description' => t('Auto-generated subscribe link.'),
  );
  $info['tokens']['subscription']['unsubscribe'] = array(
    'name' => t('Unsubscribe'),
    'description' => t('Auto-generated unsubscribe link.'),
  );
  $info['tokens']['subscription']['unsubscribe-page'] = array(
    'name' => t('Unsubscribe page'),
    'description' => t('Link to unsubscribe page.'),
  );

  // Content type tokens.
  $info['types']['subscription'] = array(
    'name' => t('Subscription'),
    'description' => t('Tokens related to subscription.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function total_subscription_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'subscription') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'subscribe':
          if (isset($data['subscribe'])) {
            $replacements[$original] = $data['subscribe'];
          }
          break;

        case 'unsubscribe':
          if (isset($data['unsubscribe'])) {
            $replacements[$original] = $data['unsubscribe'];
          }
          break;

        case 'unsubscribe-page':
          if (isset($data['unsubscribe-page'])) {
            $replacements[$original] = $data['unsubscribe-page'];
          }
          break;
      }
    }
  }
  return $replacements;
}
