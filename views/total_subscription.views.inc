<?php

/**
 * @file
 * Views integration for the Total Subscription module.
 */

/**
 * Implements hook_views_data().
 */
function total_subscription_views_data() {
  $table = array(
    'total_subscription' => array(
      'table' => array(
        'group' => 'Total Subscription',
        'base' => array(
          'field' => 'tsid',
          'title' => 'Total Subscription',
          'help' => 'Total Subscription database table',
        ),
      ),
      // Description of tsid.
      'tsid' => array(
        'title' => t('Total Subscription Id'),
        'help' => t('Total Subscription Id field'),
        // Is tsid field sortable TRUE.
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        // Filter handler for filtering records by tsid.
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      // Description of mail field.
      'mail' => array(
        'title' => t('Total Subscription Mail'),
        'help' => t('Total Subscription Mail field'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      // Description of bundle field.
      'bundle' => array(
        'title' => t('Total Subscription Bundle'),
        'help' => t('Total Subscription Bundle field'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      // Description of entity_id field.
      'entity_id' => array(
        'title' => t('Total Subscription Entity_id'),
        'help' => t('Total Subscription Entity_id field'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      // Description of bundle_and field.
      'bundle_and' => array(
        'title' => t('Total Subscription Bundle_and'),
        'help' => t('Total Subscription Bundle_and field'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      // Description of entity_id_and field.
      'entity_id_and' => array(
        'title' => t('Total Subscription Entity_id_and'),
        'help' => t('Total Subscription Entity_id_and field'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      // Description of active field.
      'active' => array(
        'title' => t('Total Subscription Active'),
        'help' => t('Total Subscription Active field'),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_boolean_operator',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      // Description of hash field.
      'hash' => array(
        'title' => t('Total Subscription Hash'),
        'help' => t('Total Subscription Hash field'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      // Description of entity_type field.
      'entity_type' => array(
        'title' => t('Total Subscription Entity_type'),
        'help' => t('Total Subscription Entity_type field'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      // Description of created field.
      'created' => array(
        'title' => t('Total Subscription Created'),
        'help' => t('Total Subscription Created field'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
      // Description of verified field.
      'verified' => array(
        'title' => t('Total Subscription Verified'),
        'help' => t('Total Subscription Verified field'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
    ),
  );
  return $table;
}

/**
 * Implements hook_views_data_alter().
 */
function total_subscription_views_data_alter(&$data) {
  $data['total_subscription']['actions'] = array(
    'title' => t('Actions'),
    'help' => t('Clickable links to actions a user may perform on a entity.'),
    'field' => array(
      'handler' => 'total_subscription_views_handler_field_actions',
      'group' => 'Total Subscription',
      'click sortable' => FALSE,
    ),
  );
  // Expose the uid as a relationship to users.
  $data['total_subscription']['users'] = array(
    'title' => t('Users'),
    'help' => t('Relate a users to the subscriptions they have placed. Works with registered users only.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'mail',
      'relationship field' => 'mail',
      'handler' => 'views_handler_relationship',
      'label' => t('Users'),
    ),
  );
  // Expose the entity_id as a relationship to nodes.
  $data['total_subscription']['nodes'] = array(
    'title' => t('Nodes'),
    'help' => t('Relate a nodes to the subscriptions. Works with nodes only.'),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'relationship field' => 'entity_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Nodes'),
    ),
  );
}
