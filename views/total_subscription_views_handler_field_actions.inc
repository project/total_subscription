<?php

/**
 * @file
 * Handler class for the new field 'actions'.
 */

/**
 * Class definition.
 */
class total_subscription_views_handler_field_actions extends views_handler_field {

  /**
   * Render function, return HTML output. Include edit and delete action.
   */
  public function render($values) {
    if (user_access('total subscription admin subscriptions permission')) {
      $params = array(
        'mail' => $values->total_subscription_mail,
        'hash' => $values->total_subscription_hash,
        'destination' => '/admin/config/user-interface/subscription-mail/subscriptions',
      );
      if ($values->total_subscription_entity_type == 'taxonomy_term') {
        $params['entity_type'] = $values->total_subscription_entity_type . '-0';
        $params['entity_id'] = urlencode(drupal_json_encode($values->total_subscription_entity_id));
        // '[]';.
        $params['node'] = urlencode(drupal_json_encode(array()));
        $params['tids'] = urlencode($values->total_subscription_entity_id);
        if ($values->total_subscription_entity_id_and != 0) {
          $params['entity_id'] = urlencode(drupal_json_encode($values->total_subscription_entity_id . '+'
            . $values->total_subscription_entity_id_and));
          $params['tids'] = urlencode(drupal_json_encode($values->total_subscription_entity_id . '+'
            . $values->total_subscription_entity_id_and));
        }
      }
      else {
        $params['entity_type'] = $values->total_subscription_entity_type . '-1';
        $params['entity_id'] = urlencode(drupal_json_encode($values->total_subscription_entity_id));
        $params['node'] = urlencode(drupal_json_encode($values->total_subscription_entity_id));
        $params['tids'] = urlencode(drupal_json_encode(array()));
        $params['destination'] .= '/nodes';
      }

      if ($values->total_subscription_active) {
        $title = t('Unsubscribe');
        $image = 'unsubscribe.png';
        $params['action'] = 'deactivate';
      }
      else {
        $title = t('Subscribe');
        $image = 'subscribe.png';
        $params['action'] = 'subscribe';
      }

      $output = l(
        theme('image',
          array(
            'path' => drupal_get_path('module', 'total_subscription') . '/images/' . $image,
            'alt' => $title,
          )
        ),
        '/total-subscription-verify',
        array(
          'attributes' => array('title' => $title),
          'query' => $this->generateLink($params),
          'html' => TRUE,
        )
      );

      $params['action'] = 'unsubscribe';
      $title = t('Delete');
      $output .= l(
        theme('image',
          array(
            'path' => drupal_get_path('module', 'total_subscription') . '/images/delete.png',
            'alt' => $title,
          )
        ),
        '/total-subscription-verify',
        array(
          'attributes' => array('title' => $title),
          'query' => $this->generateLink($params),
          'html' => TRUE,
        )
      );
      return urldecode($output);
    }
    return '';
  }

  /**
   * Query stub.
   */
  public function query() {
    // Do nothing, leave query blank, we render the contents.
  }

  /**
   * Generate subscription link.
   */
  public function generateLink($params) {
    $q = array();
    if (isset($params['mail'])) {
      $q['email'] = $params['mail'];
    }
    if (isset($params['entity_type'])) {
      $q['entity_type'] = $params['entity_type'];
    }
    if (isset($params['entity_id'])) {
      $q['entity_id'] = $params['entity_id'];
    }
    if (isset($params['tids'])) {
      $q['tids'] = $params['tids'];
    }
    if (isset($params['node'])) {
      $q['node'] = $params['node'];
    }
    if (isset($params['action'])) {
      $q[$params['action']] = $params['hash'];
    }
    if (isset($params['destination'])) {
      $q['destination'] = $params['destination'];
    }
    return $q;
  }

}
